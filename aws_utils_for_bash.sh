#!/usr/bin/env bash

# Collection of Bash functions complimenting the AWS CLI

#
# INSTALLATION
#
# Note: You need the AWS CLI installed and configured.
#
# This script is available in the following repository:
# https://codeberg.org/slu/aws-utils-for-bash
#
# Clone the repository
#
#     cd <some directory>
#     git clone https://codeberg.org/slu/aws-utils-for-bash.git
#
# Then add the following line to your ~/.bashrc:
#
# source "<some directory>/aws-utils-for-base/aws_utils_for_bash.sh"
#
#


function aws-ec2-describe-instances() {
    aws ec2 describe-instances \
        --filters Name=instance-state-name,Values=running \
        --query "Reservations[*].Instances[*].[InstanceId,PrivateIpAddress,Tags[?Key=='Name']|[0].Value]" \
        --output text
}

function aws-ec2-describe-regions() {
    aws ec2 describe-regions \
        --all-regions \
        --query "Regions" \
        --output text \
        | sort -k2,2r -k1,1 | nl | column -t
}

function aws-sts-get-caller-account-id() {
    aws sts get-caller-identity \
        --query "Account" \
        --output text
}

function aws-secretsmanager-get-secretstring() {
    aws secretsmanager get-secret-value \
        --query "SecretString" \
        --output text \
        --secret-id "$1"
}

function aws-secretsmanager-list-names() {
    aws secretsmanager list-secrets \
        --query "SecretList[].Name" \
        --output text \
        | tr "\t" "\n" \
        | sort
}

function aws-cloudformation-list-stack-names() {
    aws cloudformation list-stacks \
        --query "StackSummaries[?StackStatus!='"'DELETE_COMPLETE'"'].StackName" \
        --output text \
        | tr "\t" "\n" \
        | sort
}

function aws-cloudformation-describe-stacks() {
    aws cloudformation describe-stacks \
        --query "Stacks[].[StackName,StackStatus,CreationTime,LastUpdatedTime]" \
        --output text \
        | sort -k1 \
        | column -t \
        | grep -E "[A-Z_]+_FAILED|\$"
}


# ------------------------------------------------------------------------------
# Let you select an AWS profile to use from a menu or fzf
#
# A list of profiles is created by reading ar parsing the ~/.aws/credentials.
#
# If fzf[1] then it will be used to let you select a profile from the
# list. If you pres <ctrl>-c then nothing will be selected and the
# function will exit.
#
# Else a menu is created by printing the profiles as a numbered list
# of profile names sorted by profile name. You select a profile by
# entering the number corresponding to a profile name followed by
# <enter>. If you just press <enter> nothing will be selected and the
# function will exit.
#
# In either case, if you select a profile that is currently set, that
# is the AWS_PROFILE environment variable is set to this, then a
# message notifying you will be printed and nothing will happen.
#
# But, if you select a profile that is not currently set, then the
# AWS_PROFILE environment variable will be set and the AWS Account ID
# for the profile will be retrieved. If the Account ID cannot be
# retrieved you will be notified, but the profile will still be
# selected, else a message telling you that you switched to the
# profile will be displayed.
#
# Note: This function is meant for interactive use only and was
# originally published as a GitHub Gist[2].
#
# [1] https://github.com/junegunn/fzf
# [2] https://gist.github.com/soren/dbfd37fc2ee66dec1f686012dfa23855
#
# Arguments:
#   None
# Environment Variables:
#   AWS_PROFILE - must exist as an exported environment variable
#                 will be modified by this function
#
function aws-profile() {
    local -a profiles
    local i a profile cmd account
    local aws_creds_file="$HOME/.aws/credentials"

    if [[ ! -f "$aws_creds_file" ]]; then
        echo "AWS credentials file (${aws_creds_file}) not found!"
        return
    fi

    if hash fzf &> /dev/null; then
        profile=$(grep -oP '(?<=\[).+(?=\])' "$aws_creds_file" | sort | fzf)
        [[ -z $profile ]] && return
    else
        mapfile -t profiles < <(grep -oP '(?<=\[).+(?=\])' "$aws_creds_file" | sort)

        for ((i=0; i<${#profiles[@]}; i++)); do
            printf "%02d - %s\n" $((i+1)) "${profiles[$i]}"
        done | column

        echo
        while true; do
            echo -n "Select profile: "
            read -r a
            a=$((10#$a))
            [[ -z $a ]] || [[ $a -gt 0 && $a -le ${#profiles[@]} ]] && break;
        done

        [[ -z $a ]] && return

        profile=${profiles[$((a-1))]}
    fi

    if [[ $AWS_PROFILE == "$profile" ]]; then
        echo "The ${profile} profile is already selected."
        return
    fi

    cmd="AWS_PROFILE=${profile}"
    eval "$cmd"

    echo
    account=$(aws sts get-caller-identity --query "Account" --output text 2>&1) \
        &&  echo "Switched to AWS Profile=${profile}, Account ID=${account}" \
            || echo "Your credentials for ${profile} are invalid!"
    echo
}
